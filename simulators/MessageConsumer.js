'use strict';
var stomp = require('stomp-client');

const EventEmitter = require('events');
class MsgNotifier extends EventEmitter {}

var MessageConsumer = function MessageConsumer(){};
const msgNotifier = new MsgNotifier();

MessageConsumer.prototype.init = function init(){
    var stompClient = new stomp('localhost', 61613, 'admin', 'admin');
    stompClient.connect(function(sessionId){
      stompClient.subscribe('/topic/2019122205', function(body, headers)
      {        
        console.log(body);
        msgNotifier.emit("dag-data");
      })
    });
}

module.exports = new MessageConsumer;



