'use strict';
var Stomp = require('stomp-client');


var MessageProducer = function MessageProducer(){
  this._stompClient = null;
};

var index =0;

MessageProducer.prototype.init = function init(){
  this._stompClient = new Stomp('localhost', 61613, 'admin', 'admin');
  this._stompClient.connect(function(sessionId){
    console.log('STOMP client connected.');
  });
};

MessageProducer.prototype.sendMessage = function sendMessage(messageToPublish){
  this._stompClient.publish('/topic/2019122205', messageToPublish);
};





module.exports = new MessageProducer();